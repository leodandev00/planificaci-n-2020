<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Docente extends Model
{
    // Listar docentes maximizados
    public static function listAllDocentesMaximizados(){
        $list = DB::table('docentes')
        ->where('docentes.id','!=',32)
        ->get();
        return $list;
    }

    // Listar docentes con sus componentes
    public static function listAllDocentesConComponentes($id,$anyos){
        $list = DB::table('docentes')->select('grupos.id as id','idcomponente','componente','carrera','tipo','numero','grupos.horas as horasgrupo')
        ->leftJoin('grupos','docentes.id','=','grupos.iddocente')
        ->leftJoin('componentes','grupos.idcomponente','=','componentes.id')
        ->where([
            ['idanyos','=',$anyos],
            ['docentes.id','=',$id],
            ['docentes.verprogreso','=',1]
        ])
        ->orderBy('componentes.id')
        ->get();
        return $list;
    }

    // Listar docentes minimizados
    public static function listAllDocentesMinizados(){
        $list = DB::table('docentes')->select('id','nombre')
        ->where([
            ['docentes.id','!=',32],
            ['docentes.verprogreso','=',0]
        ])
        ->get();
        return $list;
    }

    //Maximizar o minimizar todos los docentes
    public static function updateMaxMinAll($value){
        DB::update('update docentes set verprogreso = ?', [$value]);
    }

    //Listar docentes con sus componentes para reporte de docencia directa
    public static function listAllDocentesConComponentesReporte($idAnyos){
        $list = DB::table('docentes')->select('docentes.id as iddocente', 'nombre', 'docentes.horas', 'horashorario', 
        'grupos.id as idgrupo','idcomponente', 'componente', 'carrera', 'nombrecarrera', 'tipo', 'numero', 'grupos.horas as horasgrupo', 
        'anyo', 'modalidad', 'escargahoraria')
        ->leftJoin('grupos','docentes.id','=','grupos.iddocente')
        ->leftJoin('componentes','grupos.idcomponente', '=', 'componentes.id')
        ->where([
            ['iddocente','!=',32],
            ['grupos.idanyos','=',$idAnyos]
        ])
        ->orderBy('docentes.nombre')
        ->orderBy('carrera')
        ->orderBy('anyo')
        ->orderByDesc('tipo')
        ->orderBy('numero')
        ->get();
        return $list;
    }

    //Listar docentes con sus componentes para reporte de docencia sin carga horaria
    public static function listAllDocentesConComponentesSinCargaHorariaReporte($idAnyos){
        $list = DB::table('docentes')->select('docentes.id as iddocente', 'nombre', 'docentes.horas', 'horashorario', 'grupos.id as idgrupo', 
        'idcomponente', 'componente', 'carrera', 'nombrecarrera', 'tipo', 'numero', 'grupos.horas as horasgrupo', 'anyo', 'modalidad')
        ->leftJoin('grupos','docentes.id','=','grupos.iddocente')
        ->leftJoin('componentes','grupos.idcomponente', '=', 'componentes.id')
        ->where([
            ['docentes.id','!=',32],
            ['escargahoraria','=',0],
            ['grupos.idanyos','=',$idAnyos]
        ])
        ->where('docentes.contratacion','!=','HORARIO')
        ->orderBy('docentes.nombre')
        ->orderBy('carrera')
        ->orderBy('anyo')
        ->orderByDesc('tipo')
        ->orderBy('numero')
        ->get();
        return $list;
    }

    //Listar horarios con sus componentes para reporte de docencia directa omitiendo a los administrativos
    public static function listAllDocentesHorariosConComponentesReporte($idAnyos){
        $list = DB::table('docentes')->select('docentes.id as iddocente', 'nombre', 'docentes.horas', 'horashorario', 'grupos.id as idgrupo', 
        'idcomponente', 'componente', 'carrera', 'nombrecarrera', 'tipo', 'numero', 'grupos.horas as horasgrupo', 'anyo', 'modalidad')
        ->leftJoin('grupos','docentes.id','=','grupos.iddocente')
        ->leftJoin('componentes','grupos.idcomponente', '=', 'componentes.id')
        ->where([
            ['docentes.id','!=',32],
            ['horashorario','>',0],
            ['grupos.idanyos','=',$idAnyos]
        ])
        ->where(function ($query) {
            $query->where('escargahoraria','=',1)
                    ->orWhere('docentes.contratacion','=','HORARIO');
        })
        ->orderBy('docentes.nombre')
        ->orderBy('carrera')
        ->orderBy('anyo')
        ->orderByDesc('tipo')
        ->orderBy('numero')
        ->get();
        return $list;
    }


}
