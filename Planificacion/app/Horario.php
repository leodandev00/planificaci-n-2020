<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Horario extends Model
{
    //Función para listar el horario de todas las aulas
    public static function listHorarioAula($id){
        $list = DB::table('aulas')->select('aulas.id as idaula','aulas.nombre as aulanombre','horarios.id as idhorario',
        'dia','periodo','horarios.horas as horarioshora','horarios.index as index','componentes.id as idcomponente','componentes.componente as componente',
        'carrera','anyo','grupos.id as idgrupo','grupos.tipo as tipo','grupos.numero as numero','grupos.horas as horasgrupo',
        'docentes.id as iddocente','docentes.nombre as docentenombre','horarios.idanyos as cycle')
        ->join('horarios','horarios.idaula','=','aulas.id')
        ->leftJoin('grupos','horarios.idgrupo','=','grupos.id')
        ->leftJoin('componentes','grupos.idcomponente','=','componentes.id')
        ->leftJoin('docentes','grupos.iddocente','=','docentes.id')
        ->where('idaula','=',$id)
        ->get();
        return $list;
    }

    // Función para listar el horario de todos los docentes
    public static function listHorariosDocentes($id){
        $list = DB::table('aulas')->select('horarios.id as idhorario','aulas.id as idaula','aulas.nombre as aulanombre',
        'dia','periodo','horarios.horas as horarioshora','componentes.id as idcomponente','componentes.componente as componente',
        'carrera','nombrecarrera','anyo','grupos.id as idgrupo','grupos.tipo as tipo','grupos.numero as numero','grupos.horas as horasgrupo',
        'docentes.id as iddocente','docentes.nombre as docentenombre','horarios.idanyos as cycle')
        ->join('horarios','horarios.idaula','=','aulas.id')
        ->join('grupos','horarios.idgrupo','=','grupos.id')
        ->join('componentes','grupos.idcomponente','=','componentes.id')
        ->join('docentes','grupos.iddocente','=','docentes.id')
        ->where('iddocente','=',$id)
        ->orderBy('horarios.id')
        ->get();
        return $list;
    }

    // Función para listar el horario de todos los Anyo
    public static function listHorariosAnyo($carrera,$anyo){
        $list = DB::table('aulas')->select('horarios.id as idhorario','aulas.id as idaula','aulas.nombre as aulanombre',
        'dia','periodo','horarios.horas as horarioshora','componentes.id as idcomponente','componentes.componente as componente',
        'carrera','nombrecarrera','anyo','grupos.id as idgrupo','grupos.tipo as tipo','grupos.numero as numero','grupos.horas as horasgrupo',
        'docentes.id as iddocente','docentes.nombre as docentenombre','horarios.idanyos as cycle')
        ->join('horarios','horarios.idaula','=','aulas.id')
        ->join('grupos','horarios.idgrupo','=','grupos.id')
        ->join('componentes','grupos.idcomponente','=','componentes.id')
        ->join('docentes','grupos.iddocente','=','docentes.id')
        ->where([
            ['carrera','=',$carrera],
            ['anyo','=',$anyo]
        ])
        ->orderBy('horarios.id')
        ->get();
        return $list;
    }
}
