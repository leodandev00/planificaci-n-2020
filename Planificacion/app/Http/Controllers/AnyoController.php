<?php

namespace App\Http\Controllers;

use App\Anyo;
use Illuminate\Http\Request;

class AnyoController extends Controller
{
    //Index
    public function index(){
        $collection = Anyo::all();
        return view('anyos.index',compact('collection'));
    }
    //Crear un año lectivo
    public function onCreateYear(Request $request){
        if ($request->ajax()){
            $anyo = new Anyo();
            $anyo->anyo = $request->year;
            $anyo->semestre = $request->semest;
            $anyo->save();
            return response()->json([
                "message" => $request->semest + " " + $request->year
            ]);
        }
    }
    //Obtener todos años actuales
    public function onAllYear(Request $request){
        if ($request->ajax()) {
            $collection = Anyo::all();
            return response()->json([
                "collection" => $collection
            ]);
        }
    }

    //Guardar un años por medio del modal
    public function onSaveYear(Request $request){
        if ($request->ajax()) {
            $collection = Anyo::find($request->id);
            session(['IdAnyos' => $collection->id,'IdName' => $collection->anyo,'IdSem' => $collection->semestre]);
            return response()->json([
                "title" => "Exito",
                "message" => "Se Recargara la pagina para nuevo año!"
            ]);
        }
    }
}
