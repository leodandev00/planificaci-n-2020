<?php

namespace App\Http\Controllers;

use App\Anyo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!(session()->has('paymentIntentId'))) {
            $collection = Anyo::find(1);
            session(['IdAnyos' => $collection->id,'IdName' => $collection->anyo,'IdSem' => $collection->semestre]);
        }
        return view('home');
    }
}
