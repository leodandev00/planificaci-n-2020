<?php

namespace App\Http\Controllers;

use App\Docente;
use App\Grupo;
use App\Componente;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
    //Index
    public function index(){
        return view('grupos.index');
    }

    //Crear un grupo
    public function onCreateGrup(Request $request){
        if ($request->ajax()) {
           $docente = Docente::find($request->idDocente);
           $escargaH = ($docente->horashorario > 0)? 1 : 0;
           $grupo = new Grupo();
           $grupo->idcomponente = $request->idComponente;
           $grupo->tipo = $request->tipo;
           $grupo->numero = $request->numero;
           $grupo->horas = $request->horas;
           $grupo->iddocente = $request->idDocente;
           $grupo->escargahoraria = $escargaH;
           $grupo->idanyos = session()->get('IdAnyos');
           $grupo->save();
           if ($request->idDocente == 32) {
               Componente::updateAsignarComponente($request->idComponente,1);
            }else{
                Componente::updateAsignarComponente($request->idComponente,0);
            }
           $title = "Grupo Agregado!";
           $message = "Se a agregado con exito!";
           return response()->json([
            "title" => $title,
            "message" => $message
        ]);
        }
    }

    //Actualizar un grupo
    public function onUpgradeGrup(Request $request){
        if ($request->ajax()) {
            $grupoUpgrade = Grupo::find($request->idGrupo);
            $grupoUpgrade->iddocente = $request->idDocente;
            $grupoUpgrade->save();
            if ($request->idDocente == 32) {
                Componente::updateAsignarComponente($request->idComponente,1);
            }else{
                Componente::updateAsignarComponente($request->idComponente,0);
            }
            $title = "El Grupo";
            $message = "Se a actualizado con exito!";
           return response()->json([
            "title" => $title,
            "message" => $message
            ]);
        }
    }

    //Eliminar un grupo
    public function onDeleteGrup(Request $request){
        if ($request->ajax()) {
            $grupoDelete = Grupo::find($request->idGrupo);
            $idComp = $grupoDelete->idcomponente;
            Componente::updateAsignarComponente($request->idComponente,0);
            $grupoDelete->delete();
            $title = "El Grupo";
            $message = "Se a eliminado con exito!";
           return response()->json([
            "title" => $title,
            "message" => $message,
            "id" => $idComp
            ]);
        }
    }
    
}