<?php

namespace App\Http\Controllers;

use App\Docente;
use App\Grupo;
use App\Componente;
use Illuminate\Http\Request;

class DocenteController extends Controller
{
    //Index
    public function index(){
        $collection = Docente::all();
        return view('docentes.index',compact('collection'));
    }

    //All docentes
    public function onAllDocentes(Request $request){
        if ($request->ajax()){
            $collection = Docente::all();
            return response()->json([
                'collection' => $collection
            ]);
        }
    }

    //Obtener un docente
    public function onSearchDocente(Request $request){
        if ($request->ajax()){
            $docente = Docente::find($request->id);
            return response()->json([
                'docente' => $docente
            ]);
        }
    }

    //Crear un docente
    public function onCreateDocente(Request $request){
        if ($request->ajax()) {
            $item = new Docente();
            $item->nombre = strtoupper($request->name);
            $item->horas = $request->horas;
            $item->horashorario = $request->horasHor;
            $item->contratacion = $request->contratacion;
            $item->verprogreso = 0;
            $item->save();
            return response()->json([
                'name' => ucwords(strtolower($item->nombre))
            ]);
        }
    }

    //Eliminar un docente
    public function onDeleteDocente(Request $request){
        if ($request->ajax()){
            $docente = Docente::find($request->id);
            $name = $docente->nombre;
            $docente->delete();
            return response()->json([
                'name' => ucwords(strtolower($name))
            ]);
        }
    }

    //Actualizar un docente
    public function onUpdateDocente(Request $request){
        if ($request->ajax()){
            $docente = Docente::find($request->id);
            $docente->nombre = strtoupper($request->name);
            $docente->horas = $request->horas;
            $docente->horashorario = $request->horasHor;
            $docente->contratacion = $request->contratacion;
            $docente->save();
            return response()->json([
                'name' => ucwords(strtolower($docente->nombre))
            ]);
        }
    }
    
    //Obtener todos los docentes
    public function onlist(){
        $list = Docente::all()->whereNotIn('id',32);
        return view('docentes.list_docentes',compact('list'));
    }

    //Actualizar horas de un docente
    public function upgradeDocentes(Request $request){
        if ($request->ajax()){
            $docente = Docente::find($request->id);
            $docente->horas = $request->horas;
            $docente->horashorario = $request->horarios;
            $docente->save();
            return response()->json([
                'name' => ucwords(strtolower($docente->nombre))
            ]);
        }
    }

    //Progreso Carga de los docentes
    public function onProgreso(){
        $idAnyos = session()->get('IdAnyos');
        //Todos los docentes
        $docentes = Docente::listAllDocentesMaximizados()->whereNotIn('verprogreso',0);
        $i = 0;
        $elements = null;
        foreach ($docentes as $key ) {
            $elements[$i]['docente'] = $key;
            $elements[$i]['componetes'] = Docente::listAllDocentesConComponentes($key->id,$idAnyos);
            $i++;
        }
        //$elements = Docente::listAllDocentesConComponentes(1,$idAnyos);
        $collection = collect($elements)->toArray();
        //Docentes Minimizados
        $minimizing = Docente::listAllDocentesMinizados();
        //Grupos Pedientes
        $slopes = Grupo::listAllGruposNoAsignados($idAnyos);
        return view('docentes.progreso',compact('collection','minimizing','slopes'));
    }

    public function onViewProgreso(Request $request){
        if ($request->ajax()){
            $idAnyos = session()->get('IdAnyos');
            if ($request->type == -1 || $request->type == -2){
                switch($request->type){
                    //Ver Todos los docentes maximizados
                    case -1:
                        Docente::updateMaxMinAll(1);
                    break;
                    //Ocultar todos los docentes
                    case -2:
                        Docente::updateMaxMinAll(0);
                    break;
                }
                //Manejar el id docentes para max o min
            }else{
                switch($request->option){
                    //Minimizar el docente
                    case 0:
                        $docente = Docente::find($request->type);
                        $docente->verprogreso = 0;
                        $docente->save();
                    break;
                    //Maximizar el docente
                    case 1:
                        $docente = Docente::find($request->type);
                        $docente->verprogreso = 1;
                        $docente->save();
                    break;
                }
            }
            //Todos los docentes
            $docentes = Docente::listAllDocentesMaximizados()->whereNotIn('verprogreso',0);
            $i = 0;
            $collection = null;
            if ($docente =! null){
                foreach ($docentes as $key ) {
                    $collection[$i]['docente'] = $key;
                    $collection[$i]['componetes'] = Docente::listAllDocentesConComponentes($key->id,$idAnyos);
                    $i++;
                }
            }
            //Docentes Minimizados
            $minimizing = Docente::listAllDocentesMinizados();
            return response()->json([
                'collection' => $collection,
                'minimizing' => $minimizing
            ]);
        }
    }

    //Eliminar grupo de un docente
    //Desasignar un grupo
    public function onDeleteProgresoGrup(Request $request){
        if ($request->ajax()) {
            $grupoUpgrade = Grupo::find($request->idGrupo);
            $idComp = $grupoUpgrade->idcomponente;
            $grupoUpgrade->iddocente = $request->idDocente;
            $grupoUpgrade->save();

            Componente::updateAsignarComponente($idComp,1);

            $idAnyos = session()->get('IdAnyos');
            //Todos los docentes
            $docentes = Docente::listAllDocentesMaximizados()->whereNotIn('verprogreso',0);
            $i = 0;
            $collection = null;
            if ($docente =! null){
                foreach ($docentes as $key ) {
                    $collection[$i]['docente'] = $key;
                    $collection[$i]['componetes'] = Docente::listAllDocentesConComponentes($key->id,$idAnyos);
                    $i++;
                }
            }
            //Docentes Minimizados
            $minimizing = Docente::listAllDocentesMinizados();
            //Grupos Pedientes
            $slopes = Grupo::listAllGruposNoAsignados($idAnyos);

            return response()->json([
                'collection' => $collection,
                'minimizing' => $minimizing,
                'slopes' => $slopes
            ]);
        }
    }

    //Carga Academica
    public function cargaAcademica(){
        $idAnyos = session()->get('IdAnyos');
        $elements = Docente::listAllDocentesConComponentesReporte($idAnyos);
        $collection = collect($elements)->toArray();
        return view('docentes.carga_academica',compact('collection'));
    }

    //Carga Academica Sin Carga Horaria
    public function cargaAcademicaSinCargaHoraria(){
        $idAnyos = session()->get('IdAnyos');
        $elements = Docente::listAllDocentesConComponentesSinCargaHorariaReporte($idAnyos);
        $collection = collect($elements)->toArray();
        return view('docentes.carga_academica_sin_carga_horaria',compact('collection'));
    }

    //Carga Academica Horaria
    public function cargaAcademicaHoraria(){
        $idAnyos = session()->get('IdAnyos');
        $elements = Docente::listAllDocentesHorariosConComponentesReporte($idAnyos);
        $collection = collect($elements)->toArray();
        return view('docentes.carga_academica_horaria',compact('collection'));
    }

    //Carga Academica Generica
    public function cargaAcademicaGenerica(){
        $idAnyos = session()->get('IdAnyos');
        $elements = Docente::listAllDocentesHorariosConComponentesReporte($idAnyos);
        $collection = collect($elements)->toArray();
        return view('docentes.carga_academica_generica',compact('collection'));
    }
}