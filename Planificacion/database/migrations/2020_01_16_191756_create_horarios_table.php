<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idaula')->unsigned();
            $table->foreign('idaula')->references('id')->on('aulas');
            $table->string('dia');
            $table->string('periodo');
            $table->bigInteger('idgrupo')->unsigned();
            $table->foreign('idgrupo')->references('id')->on('grupos');
            $table->integer('horas');
            $table->integer('index');
            $table->unsignedBigInteger('idanyos');
            $table->foreign('idanyos')->references('id')->on('anyos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
