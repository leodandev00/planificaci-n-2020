//Variables Globales y Funciones
var conT = 0;
var conP = 0;
var idA = 0;
var value = 0;
var tG = -1;

//Funcion Globales
function capitalizeEachWord(str) { 
  let words = str.split(" "); 
  let arr = []; 
  for (i in words) { 
    temp = words[i].toLowerCase(); 
    temp = temp.charAt(0).toUpperCase() + temp.substring(1); 
    arr.push(temp); 
  }
  return arr.join(" "); 
}

//Actualizar horas docentes
function updatehoursDoc(id){
    let horasdocente = $("#horasidd" + id).val();
    let hhdocente = $("#horashorarioidd" + id).val();
    let parametros = {
      "_token": $('meta[name="csrf-token"]').attr('content'),
      "id" : id,
      "horas" : horasdocente,
      "horarios" : hhdocente
  };
  $.ajax({
    data: parametros,
    url: route('docentes.upgrade').template,
    type: 'POST',
    success: function (response) {
      toastr.success('Sus horas han sido actualizadas!', "Docente: " + response.name);
    },
    error: function (object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

//Progreso Carga de los docentes
//Manejar todos los docentes
function onProgressAll(type){
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "type" : type
  };
  $.ajax({
    data: parametros,
    url: route('docentes.progreso.view').template,
    type: 'POST',
    success: function (response){
      onChargeDisplayElements(response)
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}
//Maximizar un docente en especifico
function onProgressMaxD(id){
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "type" : id,
    "option" : 1
  };
  $.ajax({
    data: parametros,
    url: route('docentes.progreso.view').template,
    type: 'POST',
    success: function (response){
      onChargeDisplayElements(response);
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}
//Minimizar un docente en especifico
function onProgressMinD(id){
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "type" : id,
    "option" : 0
  };
  $.ajax({
    data: parametros,
    url: route('docentes.progreso.view').template,
    type: 'POST',
    success: function (response){
      onChargeDisplayElements(response);
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

//Quitar un grupo asignado de un docente
function onProgressGrupDelete(id){
  console.log(id)
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "idGrupo" : id,
    "idDocente" : 32
  };
  $.ajax({
    data: parametros,
    url: route('docentes.progreso.deleteGrup').template,
    type: 'POST',
    success: function (response){
      let tbody = document.getElementById('table-tbody');
      let slopes = response.slopes;
      let part0 = "";
      let part1 = "<tr>";
      let part2 = "";
      let part3 = "";
      let part4 = "";
      let part5 = "";
      let part6 = "</tr>";
      tbody.innerHTML = "";
      onChargeDisplayElements(response);
      //Actualizar lista de grupos no asignados
      for (let item = 0; item < slopes.length; item++) {

        part2 = "<td class='p-1' hidden>" + slopes[item].id + "</td>";
        part3 = "<td class='p-1'>" + slopes[item].componente + "</td>";
        part4 = "<td class='p-1'>" + slopes[item].nombrecarrera + "</td>";
        part5 = "<td class='p-1 text-center'>" + slopes[item].horasgrupo + "</td>";

        part0 += part1 + part2 + part3 + part4 + part5 + part6;
        
      }
      tbody.innerHTML = part0 ;
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

// Cargar los datos en la vista del navegador
function onChargeDisplayElements(response){
  //Lista Minimizada
  let collection = response.collection;
  let min = document.getElementById('containerList');
  min.innerHTML = "";
  //Lista Maximizada
  let minimizing = response.minimizing;
  let max = document.getElementById('containerTeacher');
  max.innerHTML = "";

  for (let index = 0; index < minimizing.length; index++) {
    min.innerHTML += "<a href='javascript:onProgressMaxD(" + minimizing[index].id + ")' class='list-group-item list-group-item-action p-2'>" + minimizing[index].nombre + "</a>";
  }

  let horas = 0;
  let suma = 0;
  let index = 0;
  if (collection != null){
    while ( index < collection.length) {
      let part1 = "<div class='col-md-3'><div class='card p-0 mb-3'><div class='card-body p-1'>";
      let part2 = "<table class='table'><thead><tr class='thead bg-light'><th class='p-1'>" + capitalizeEachWord(collection[index].docente.nombre);
      let part3 = "</th><th class='p-1'>G</th><th colspan='0' class='p-1'>H</th><th class='p-1'>" + "<a href='javascript:onProgressMinD(" + collection[index].docente.id + ")'>";
      let part4 = "<i class='icon ion-md-remove'></i></a></th></tr></thead><tbody>";
      let part5 = "";
      let part6 = "";
      horas = collection[index].docente.horas + collection[index].docente.horashorario;
      suma = 0;
      collection[index].componetes.forEach(item => {
        if (item.componente != null) {
          part5 += "<tr><td width='70%' class='p-1'>" + item.carrera + "-" + item.componente + "</td><td class='p-1'>G" + item.tipo + item.numero + "</td><td class='p-1'>" + item.horasgrupo + "</td><td class='p-1'><a href='javascript:onProgressGrupDelete(" + item.id + ")'><i class='icon ion-md-trash'></i></a></td></tr>";
          suma += item.horasgrupo;
        }  
      });
      if ((horas - suma) < 0) {
        part6 = "</tbody></table></div><div class='card-footer p-1 text-white text-right bg-danger'>";
      }else if ((horas - suma) == 0) {
        part6 = "</table></div><div class='card-footer p-1 text-white text-right bg-success'>";
      }else if ((horas - suma) > 0) {
        part6 = "</table></div><div class='card-footer p-1 text-white text-right bg-primary'>";
      }
      max.innerHTML += part1 + part2 + part3 + part4 + part5 + part6 + "<b>" + suma + " / " + horas + "</b></div></div></div>";
      index++;
    }
  }
}

//Manejar los grupos

//Actualizar las horas de un componente especifico
function updatehoursComp(id){
  let horasT = $("#horasidT" + id).val();
  let horasP = $("#horasidP" + id).val();
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "id" : id,
    "T" : horasT,
    "P" : horasP
  };
  $.ajax({
    data: parametros,
    url: route('componentes.HoursComp').template,
    type: 'POST',
    success: function (response){
      toastr.success("Horas Actualizadas con exito!",response.nombre);
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

//Visualizar los grupos del componete
function onSetViewGrup(id){
  let view = document.getElementById('containerGrup');
  let viewTitle = document.getElementById('titleComp');
  let viewContainer = document.getElementById('containerGrp');
  conP = 0;
  conT = 0;
  if(idA == 0){
    idA = id;
  }
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "id" : id
  };
  $.ajax({
    data: parametros,
    url: route('componentes.searchAtOne').template,
    type: 'POST',
    success: function (response){
      let componente = response.componente;
      let docentes = response.docentes;
      let grupos = response.grupos;
      //Selecionamos la columna
      if (idA != componente.id){
        $("#color" + idA).removeClass('bg-warning');
        $("#color" + componente.id).addClass('bg-warning');
        idA = componente.id;
      }else{
        $("#color" + componente.id).addClass('bg-warning');
      }
      //Limpiamos pantalla
      viewTitle.innerHTML = "";
      viewContainer.innerHTML = "";
      //Colocamos titulo del componente
      viewTitle.innerHTML = componente.componente;
      //Almacenamos las horas y id
      document.getElementById('idComp').value = componente.id;
      document.getElementById('hoursCompT').value = componente.horasteoricas;
      document.getElementById('hoursCompP').value = componente.horaspracticas;
      //Colocamos los grupos asigandos al componete
      let part1;
      let part2;
      let part3;
      let part4;
      let part5;
      if (grupos != null){
        for (let index = 0; index < grupos.length; index++) {
          if (grupos[index].tipo == "T"){
            conT++;
          }else if (grupos[index].tipo == "P"){
            conP++;
          }
          part1 = "<tr><td style='text-align: center'>G"+ grupos[index].tipo + grupos[index].numero + "</td>";
          part2 = "<td style='text-align: center'>" + grupos[index].horas + "</td><td><select id='idDocG"+ grupos[index].id +"' class='custom-select' style='height: 30px; font-size: small;'>";
          for (let item = 0; item < docentes.length; item++) {
            if (docentes[item].id == grupos[index].iddocente) {
              part3 += "<option value='" + docentes[item].id + "' selected>" + docentes[item].nombre + "</option>";
            }else{
              part3 += "<option value='" + docentes[item].id + "'>" + docentes[item].nombre + "</option>";
            }
          }
          part4 = "</select></td><td><a href='javascript:onSetGrupUpgrade(" + grupos[index].id + ")' class='btn btn-success btn-sm data-toggle='tooltip' data-placement='top' title='Actualizar''><i class='icon ion-md-sync'></i></a>";
          part5 = " <a href='javascript:onSetGrupDelete(" + grupos[index].id + ")' class='btn btn-danger btn-sm' data-toggle='tooltip' data-placement='top' title='Eliminar'><i class='icon ion-md-trash'></i></a></td></tr>";
          viewContainer.innerHTML += part1 + part2 + part3 + part4 + part5;
        }
      }
      //Mostramos
      view.hidden = false;
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

//Crear Form de nuevo grupo
function onSetGrup(type){
  let part1;
  let part2;
  let part3;
  let part4;
  let part5;
  let viewContainer = document.getElementById('containerGrp');
  let hours;
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
  };
  let title = $('#titleComp').html();
  let bandera = true;
  switch(type){
    case 0:
      if ($('#hoursCompT').val() <= 0) {
        toastr.warning("Grupo Teorico no hay horas disponibles",title);
        bandera = false;
      }
      break;
    case 1:
      if($('#hoursCompP').val() <= 0){
        toastr.warning("Grupo Practico no hay horas disponibles",title);
        bandera = false;
      }
      break;
  }
  if(bandera){
    $.ajax({
      data: parametros,
      url: route('componentes.getDocentes').template,
      type: 'POST',
      success: function (response){
        let docentes = response.docentes;
        switch(type){
          case 0:
            conT += 1;
            value = conT;
            tG = 0;
            hours = document.getElementById('hoursCompT').value;
            part1 = "<tr><td style='text-align: center'>GT" + conT + "</td>";
            break;
          case 1:
            conP += 1;
            value = conP;
            tG = 1;
            hours = document.getElementById('hoursCompP').value;
            part1 = "<tr><td style='text-align: center'>GP" + conP + "</td>";
            break;
        }
        let object = new Array();
        object[0] = tG;
        object[1] = value;
        part2 = "<td style='text-align: center'>" + hours + "</td><td><select id='idDoc" + tG + value + "' class='custom-select' style='height: 30px; font-size: small;'>";
        part3 = "<option value='32'>NO ASIGNADO</option>";
        for (let item = 0; item < docentes.length; item++) {
          part3 += "<option value='" + docentes[item].id + "'>" + docentes[item].nombre + "</option>";
        }
        part4 = "</select></td><td id='" + tG + value + "'><a href='javascript:onSetGrupCreate(" + object + ")' class='btn btn-success btn-sm' data-toggle='tooltip' data-placement='top' title='Guardar'><i class='icon ion-md-wallet'></i></a>";
        part5 = "<a href='javascript:onGrupDelete("+ object +")' class='btn-delete btn btn-danger btn-sm' data-toggle='tooltip' data-placement='top' title='Eliminar'><i class='icon ion-md-trash'></i></a></td></tr>";
        viewContainer.innerHTML += part1 + part2 + part3 + part4 + " " + part5;
      },
      error: function(object,type,message){
        if (type == "error"){
          toastr.error(message,capitalizeEachWord(type));
        }else if (type == "timeout"){
          toastr.info(message,capitalizeEachWord(type));
        }else{
          toastr.warning(message,capitalizeEachWord(type));
        }
      }
    });
  }
}

//Crear un nuevo grupo
function onSetGrupCreate(key,number){
  let type = "";
  switch(key){
    case 0:
      type = "T";
      break;
    case 1:
      type = "P";
      break;
  }
  let idComponente = document.getElementById('idComp').value;
  let hours = document.getElementById('hoursComp' + type).value;
  let idDocente = $("#idDoc"+key+number).val();
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "idComponente" : idComponente,
    "tipo" : type,
    "numero" : number,
    "horas" : hours,
    "idDocente" : idDocente,
    "idAnyos" : 1
  };
  $.ajax({
    data: parametros,
    url: route('grupos.create').template,
    type: 'POST',
    success: function (response){toastr.info(response.message,response.title);},
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

//Actualizar docente de un grupo
function onSetGrupUpgrade(id){
  let idDocente = $("#idDocG"+id).val();
  let idComponente = document.getElementById('idComp').value;
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "idGrupo" : id,
    "idDocente" : idDocente,
    "idComponente" : idComponente
  };
  $.ajax({
    data: parametros,
    url: route('grupos.upgrade').template,
    type: 'POST',
    success: function (response){toastr.info(response.message,response.title);},
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}


//Eliminar un grupo
function onSetGrupDelete(id){
  let idDocente = $("#idDocG"+id).val();
  let idComponente = document.getElementById('idComp').value;
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "idGrupo" : id,
    "idDocente" : idDocente,
    "idComponente" : idComponente
  };
  $.ajax({
    data: parametros,
    url: route('grupos.delete').template,
    type: 'POST',
    success: function (response){toastr.info(response.message,response.title);onSetViewGrup(response.id);},
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
}

function onGrupDelete(key,number){
  let type = "";
  switch(key){
    case 0:
      type = "T";
      conT -= 1;
      break;
    case 1:
      type = "P";
      conP -= 1;
      break;
  }
}