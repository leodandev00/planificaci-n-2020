var collection;

function capitalizeEachWord(str) { 
    let words = str.split(" "); 
    let arr = []; 
    for (i in words) { 
      temp = words[i].toLowerCase(); 
      temp = temp.charAt(0).toUpperCase() + temp.substring(1); 
      arr.push(temp); 
    }
    return arr.join(" "); 
  } 
  
//Modal para elegir el año y ciclo lectivo
function onSetCycleYear(){
    let route = $("#route").val();
    let selector = document.getElementById("yearSelect");
    let yearID = $("#idAnyoL").val();
    let parametros = {
        "_token": $('meta[name="csrf-token"]').attr('content')
    };
    selector.innerHTML = "";
    selector.innerHTML = "<option disabled value='-1'>Selecione...</option>";
    $.ajax({
        data: parametros,
        url: route,
        type: 'POST',
        success: function (response){
          collection = collection;
            response.collection.forEach(element => {
                if (yearID == element.id){
                  selector.innerHTML += "<option value='" + element.id + "' selected>" + element.semestre + " del " + element.anyo + "</option>";
                }else{
                  selector.innerHTML += "<option value='" + element.id + "'>" + element.semestre + " del " + element.anyo + "</option>";
                }
            });
        },
        error: function(object,type,message){
          if (type == "error"){
            toastr.error(message,capitalizeEachWord(type));
          }else if (type == "timeout"){
            toastr.info(message,capitalizeEachWord(type));
          }else{
            toastr.warning(message,capitalizeEachWord(type));
          }
        }
    });
}

//Hacer segun elecion usuario
function onCloseModal(){
  let route = $("#routeSel").val();
  let id = $("#yearSelect").val();
  let parametros = {
    "_token": $('meta[name="csrf-token"]').attr('content'),
    "id" : id
  };
  $.ajax({
    data: parametros,
    url: route,
    type: 'POST',
    success: function (response){
      toastr.success(response.message,response.title);
      window.document.location.href = $("#home")[0].baseURI;
    },
    error: function(object,type,message){
      if (type == "error"){
        toastr.error(message,capitalizeEachWord(type));
      }else if (type == "timeout"){
        toastr.info(message,capitalizeEachWord(type));
      }else{
        toastr.warning(message,capitalizeEachWord(type));
      }
    }
  });
  $("[data-dismiss=modal]").trigger({ type: "click" });
}