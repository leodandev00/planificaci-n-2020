<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Plan') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/appScript.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}" id="home">
                    {{ config('app.name', 'Plan') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @if (Route::has('login'))
                        @auth
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Operaciones
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ route('docentes.list') }}">Listar Docentes</a>
                                  <a class="dropdown-item" href="{{ route('docentes.progress') }}">Progreso carga</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="{{ route('componentes.IS') }}">Grupos IS</a>
                                  <a class="dropdown-item" href="{{ route('componentes.IT') }}">Grupos IT</a>
                                  <a class="dropdown-item" href="{{ route('componentes.SE') }}">Grupos Servicio</a>
                                  <a class="dropdown-item" href="{{ route('componentes.UALN') }}">Grupos UALN</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="{{ route('horario.crear') }}">Trabajar horarios</a>
                                  <button type="button" class="dropdown-item" onclick="onSetCycleYear();" data-toggle="modal" data-target="#staticBackdrop">Ciclo Academico</button>
                                </div>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Reportes
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item"   href="{{ route('docentes.cargaAcademica') }}">Carga académica</a>
                                  <a class="dropdown-item"   href="{{ route('docentes.cargaAcademicaSinCargaHoraria') }}">Carga académica (sin carga horaria)</a>
                                  <a class="dropdown-item"   href="{{ route('docentes.cargaAcademicaHoraria') }}">Carga académica (sólo carga horaria)</a>
                                  <a class="dropdown-item"   href="{{ route('docentes.cargaAcademicaGenerica') }}">Carga académica horaria genérica </a>
                                  <a class="dropdown-item"   href="{{ route('componentes.cargaAcademica') }}">Carga académica por componentes</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item"   href="{{ route('horario.docente') }}">Horario por docente</a>
                                  <a class="dropdown-item"   href="{{ route('horario.anyo') }}">Horario por año</a>
                                  <a class="dropdown-item"   href="{{ route('horario.aula') }}">Horario por aula</a>
                                </div>
                              </li>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Crear
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item"   href="{{ route('docentes') }}">Docente</a>
                                  <a class="dropdown-item"   href="{{ route('aulas') }}">Aula</a>
                                  <a class="dropdown-item"   href="{{ route('componentes') }}">Componente</a>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item"   href="{{ route('anyos') }}">Año</a>
                                </div>
                              </li>
                            </ul>
                        @endauth
                    @endif
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="icon ion-md-contact"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="icon ion-md-log-out"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @if (Route::has('login'))
            @auth
                <main class="py-4">
                    @yield('content')
                </main>
                </div>
                <!-- Input -->
                <input type="text" id="route" hidden disabled value="{{ route('anyos.all') }}">
                <input type="text" id="routeSel" hidden disabled value="{{ route('anyos.save') }}">
                <input type="number" id="idAnyoL" hidden disabled value="{{Session::get('IdAnyos')}}"/>
                <!-- Modal -->
                <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Ciclo Académico</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="icon ion-md-close-circle"></i>
                        </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="inputYear1">Año Académico</label>
                                    <select class="form-control" required id="yearSelect">
                                        <option selected disabled value="-1">Selecione...</option>
                                    </select>
                                </div>
                                <div class="row justify-content-center">
                                    <button type="button" class="btn btn-primary" onclick="onCloseModal()"><i class="icon ion-md-save"></i> Cargar...</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
                @yield('script')
            @endauth
            @guest
                <main class="py-4">
                    @yield('content_login')
                </main>
            @endguest
        @endif
</body>
</html>
