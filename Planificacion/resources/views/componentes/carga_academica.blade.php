@extends('layouts.app')

@section('content')
<div class="container">
    <center>
      <h4>Universidad Nacional Autónoma de Nicaragua</h4>
      <h5>UNAN - León</h5>
      <h6>Departamento de Computación</h6>
      <h6>Reporte de Docencia Directa {{Session::get('IdSem')}} {{Session::get('IdName')}} (Carga Académica por componente)</h6>
    </center>
    <hr />
    <div class="row">
        @php
            $count = count($collection);
            $i = 0;
          @endphp
          @while ($i < $count)
            @if ($collection[$i]->componente != "")
                @php
                    $idActual = $collection[$i]->idcomponente;
                @endphp
                <!-- Desde acá se puede configurar la cantidad de elementos que se deben mostrar -->
                <div class="col-md-12">
                    <div class="card p-0 mb-3">
                        <div class="card-body p-1">
                            <table class="table m-0">
                                <thead>
                                    <tr class="thead bg-light">
                                        <th>
                                            {{$collection[$i]->componente}}
                                            @if ($collection[$i]->modalidad == 'VIRTUAL')
                                                <span class="text-info font-weight-bold">{{$collection[$i]->modalidad}}</span>
                                            @endif
                                        </th>
                                        <th class="pl-0">Carrera</th>
                                        <th class="pl-0">Año</th>
                                        <th class="pl-0">Grupo</th>
                                        <th class="pl-0">Horas</th>
                                    </tr>
                                </thead>
                                @php
                                    // Inicialización de las horas asignadas al docente
                                    $suma = 0;
                                    // Si el docente tiene horas asignadas se muestran con el siguiente ciclo
                                    $con = 0;
                                @endphp
                                <tbody>
                                    @foreach ($collection as $item)
                                    @if ($item->idcomponente == $collection[$i]->idcomponente)
                                        @if ($item->componente != "")
                                            <tr>
                                                <td width="50%" class="p-1">{{$item->nombre}}
                                            @if ($suma == 0 && $item->modalidad != 'VIRTUAL')
                                                <span class="text-danger font-weight-bold">[PROFESOR PRINCIPAL]</span>
                                            @endif
                                                </td>
                                                <td class="p-1">{{$item->nombrecarrera}}</td>
                                                <td class="p-1">{{$item->anyo}}</td>
                                                <td class="p-1">G{{$item->tipo}} {{$item->numero}}</td>
                                                <td class="p-1">{{$item->horasgrupo}}</td>
                                            </tr>
                                        @endif
                                        @php
                                            $con++;
                                            $suma = $suma + $item->horasgrupo;
                                        @endphp
                                    @endif
                                    @endforeach
                                    @if ($con == 0)
                                        @php
                                            $i++;
                                        @endphp
                                    @else
                                        @php
                                            $i += $con;
                                        @endphp
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- Mostraremos -->
                        <div class="card-footer p-1 text-white text-right bg-success">
                            <b>{{$suma}}</b>
                        </div>
                    </div>
                </div>
            @endif
            @php
                $i++;
            @endphp
        @endwhile
    </div>
</div>
<br />
<br />
@endsection