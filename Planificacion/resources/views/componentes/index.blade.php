@extends('layouts.app')

@section('content')
<h6 class="btn btn-secondary btn-lg btn-block">Componente Curricular del Departamento de Computación</h6>
<div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <p class="row justify-content-center"><strong>Acciones</strong>  <i class="icon ion-md-hammer"></i></p>
        <br />
        <div class="list-group">
            <a href="javascript:onKeyComponente(0)" class="list-group-item list-group-item-action p-2 text-center">CREAR  <i class="icon ion-md-add-circle"></i></a>
            <a href="" data-toggle="modal" data-target="#exampleModalEditar" class="list-group-item list-group-item-action p-2 text-center">EDITAR  <i class="icon ion-md-create"></i></a>
            <a href="" data-toggle="modal" data-target="#exampleModalEliminar" class="list-group-item list-group-item-action p-2 text-center">ELIMINAR  <i class="icon ion-md-trash"></i></a>
        </div>
    </div>
    <div class="col-md-10">
        <div id="containerViewCompInicio" class="container">
            
        </div>
        <div id="containerViewCompCreate" class="container" hidden>
            <div class="row">
              <input type="number" id="idComponente" hidden="hidden" disabled="disabled" value="">
                <div class="col-md-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal">
                                <div class="form-group row">
                                    <label for="inputNameComponete" class="col-sm-2 control-label">Nombre Componente</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputNameComponente" placeholder="PROGRAMACION ORIENTADA A LA WEB">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputComponenteModalidad" class="col-sm-2 control-label">Modalidad</label>
                                    <div class="col-sm-8">
                                        <select id="inputComponenteModalidad" class="form-control">
                                            <option value="-1" selected>Selecione ...</option>
                                            <option value="1">PRESENCIAL</option>
                                            <option value="2">VIRTUAL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputHorasTeoricasComp" class="col-sm-2 control-label">Horas Teoricas</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="horasTComp" value="0" class="form-control" id="inputHorasTeoricasComp">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputHorasPracticasComp" class="col-sm-2 control-label">Horas Practicas</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="horasPComp" value="0" class="form-control" id="inputHorasPracticasComp">
                                    </div>
                                </div>
                                <fieldset>
                                    <div class="row">
                                      <legend class="col-form-label col-sm-2 pt-0">Carrera</legend>
                                      <div class="col-sm-6">
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                                          <label class="form-check-label" for="gridRadios1">
                                            INGENIERIA EN SISTEMAS DE INFORMACION
                                          </label>
                                        </div>
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                                          <label class="form-check-label" for="gridRadios2">
                                            INGENIERIA EN TELEMATICA
                                          </label>
                                        </div>
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3">
                                          <label class="form-check-label" for="gridRadios3">
                                            TECNICO SUPERIOR EN DESARROLLO WEB
                                          </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios4" value="option4">
                                            <label class="form-check-label" for="gridRadios4">
                                            OFERTA OPTATIVAS DE LA FFCCTT
                                            </label>
                                          </div>
                                          <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios5" value="option5">
                                            <label class="form-check-label" for="gridRadios5">
                                            TS ESTADISTICA DE SALUD
                                            </label>
                                          </div>
                                          <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios6" value="option6">
                                            <label class="form-check-label" for="gridRadios6">
                                            ENFERMERIA
                                            </label>
                                          </div>
                                          <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios7" value="option7">
                                            <label class="form-check-label" for="gridRadios7">
                                            BIOANALISIS CLINICO
                                            </label>
                                          </div>
                                          <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios8" value="option8">
                                            <label class="form-check-label" for="gridRadios8">
                                            BIOLOGIA
                                            </label>
                                          </div>
                                      </div>
                                    </div>
                                  </fieldset>
                                  <div class="form-group row">
                                    <label for="inputCicloComp" class="col-sm-2 control-label">Ciclo</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="cicloComp" value="0" class="form-control" id="inputCicloComp">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputYearComp" class="col-sm-2 control-label">Año</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="yearComp" min="1" max="5" value="0" class="form-control" id="inputYearComp">
                                    </div>
                                </div>
                                  <br />
                                <div class="form-group row" >
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <button onclick="onSendDataComp()" id="btnSendComp" type="button" class="btn btn-success btn-lg btn-block">Enviar Datos</button>
                                        <button onclick="onUpdateDataComp()" id="btnUpdateComp" type="button" class="btn btn-primary btn-lg btn-block" hidden>Actualizar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- Modal Editar-->
<div class="modal fade" id="exampleModalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Componente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-CompEdit" class="col-form-label">Nombre Componente:</label>
              <select id="recipient-CompEdit" class="form-control">
                  <option selected>Selecione ...</option>
                  @foreach ($collection as $item)
                    <option value="{{$item->id}}">{{$item->componente}}</option>
                  @endforeach
                </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button id="btnActionCompEdit" onclick="btnActionCompEdit()" type="button" class="btn btn-primary">Editar <i class="icon ion-md-create"></i></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Eliminar-->
<div class="modal fade" id="exampleModalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Componente Eliminar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-comp-delete" class="col-form-label">Nombre Componente:</label>
              <select id="recipient-comp-delete" class="form-control">
                  <option selected>Selecione ...</option>
                  @foreach ($collection as $item)
                    <option value="{{$item->id}}">{{$item->componente}}</option>
                  @endforeach
                </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
            <button id="btnActionCompDelete" onclick="btnActionCompDelete()" type="button" class="btn btn-danger">Si <i class="icon ion-md-trash"></i></button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
@routes
<script src="{{ asset('js/scriptElements.js') }}"></script>
@endsection