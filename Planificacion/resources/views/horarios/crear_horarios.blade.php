@extends('layouts.app')

@section('content')
<div class="container-fluid"> 
    <div class="row">
      <div class="col-md-2">
        <h5>Aulas y Laboratorios</h5>
        <div class="list-group">
          @foreach ($aulas as $item)
            <a href="javascript:showAula({{$item->id}})" class="list-group-item list-group-item-action p-2">{{$item->nombre}}</a>
          @endforeach
        </div>
      </div>
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-3"><label><input type="checkbox" id="idChoqueDocente" name="idChoqueDocente"> Choque de docentes</label></div>
          <div class="col-md-5"><label><input type="checkbox" id="idChoqueComponente" name="idChoqueComponente"> Choque de grupos del mismo componente</label></div>
          <div class="col-md-4"><label><input type="checkbox" id="idChoqueGrupo" name="idChoqueGrupo"> Choque de grupos del mismo año</label></div>
        </div>
        <div id="containerView" class="col-md-12" style="overflow-y: scroll; height: 550px; border: solid 0px;">
        </div>
      </div>
      <div class="col-md-2">
        <h5>Grupos</h5>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="is-tab" data-toggle="tab" href="#isgroup" role="tab" >ISI</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="it-tab" data-toggle="tab" href="#itgroup" role="tab">IT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="sei-tab" data-toggle="tab" href="#seigroup" role="tab" onclick="filtrarGrupos('SEI', 0,'#gruposSEI')">SEI</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="isgroup" role="tabpanel">
            <br />
            <div class="btn-group" role="group">
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IS', 1, '#gruposISAnyo')">I</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IS', 2, '#gruposISAnyo')">II</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IS', 3, '#gruposISAnyo')">III</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IS', 4, '#gruposISAnyo')">IV</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IS', 5, '#gruposISAnyo')">V</button>
            </div>
            <br />
            <br />
            <div id="gruposISAnyo" style="overflow-y: scroll; height: 400px; border: solid 0px;"></div>
          </div>
          <div class="tab-pane fade" id="itgroup" role="tabpanel">
            <br />
            <div class="btn-group" role="group">
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IT', 1, '#gruposITAnyo')">I</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IT', 2, '#gruposITAnyo')">II</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IT', 3, '#gruposITAnyo')">III</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IT', 4, '#gruposITAnyo')">IV</button>
              <button type="button" class="btn btn-secondary" onclick="filtrarGrupos('IT', 5, '#gruposITAnyo')">V</button>
            </div>
            <br />
            <br />
            <div id="gruposITAnyo" style="overflow-y: scroll; height: 400px; border: solid 0px;"></div>
          </div>
          <div class="tab-pane fade" id="seigroup" role="tabpanel">
            <br />
            <div id="gruposSEI" style="overflow-y: scroll; height: 400px; border: solid 0px;">Aqui Incompleto</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br />
  <br />
@endsection
@section('script')
@routes
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/scriptHorarios.js') }}"></script>
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection