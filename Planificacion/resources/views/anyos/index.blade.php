@extends('layouts.app')

@section('content')
<h6 class="btn btn-secondary btn-lg btn-block">Año Academico</h6>
<div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <p class="row justify-content-center"><strong>Acciones</strong>  <i class="icon ion-md-hammer"></i></p>
        <br />
        <div class="list-group">
            <a href="javascript:onStart(2)" class="list-group-item list-group-item-action p-2 text-center"><i class="icon ion-md-add-circle"></i> CREAR</a>
        </div>
    </div>
    <div class="col-md-10">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4" id="containerViewAnyo" hidden>
                    <div class="form-row">
                        <label for="validationCustom01">Año Academico</label>
                        <input type="number" class="form-control" id="validationCustom01" value="2020">
                    </div>
                <div class="form-row">
                    <label for="validationCustom03">Semestre Academico</label>
                    <select class="custom-select" id="validationCustom03">
                        <option selected disabled value="">Selecione...</option>
                        <option value="1">I Semestre Academico</option>
                        <option value="2">II Semestre Academico</option>
                    </select>
                </div>
                <br /><br />
                <div class="row justify-content-center">
                    <a href="javascript:onCreateYear()" class="btn btn-primary"><i class="icon ion-md-save"></i> Crear</a>
                </div>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
@section('script')
@routes
<script src="{{ asset('js/scriptElements.js') }}"></script>
@endsection