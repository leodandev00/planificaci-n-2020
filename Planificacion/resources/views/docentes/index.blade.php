@extends('layouts.app')

@section('content')
<h6 class="btn btn-secondary btn-lg btn-block">Docente del Departamento de Computación</h6>
<div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <p class="row justify-content-center"><strong>Acciones</strong>  <i class="icon ion-md-hammer"></i></p>
        <br />
        <div class="list-group">
            <a href="javascript:onKeyDocente(0)" class="list-group-item list-group-item-action p-2 text-center">CREAR  <i class="icon ion-md-add-circle"></i></a>
            <a href="" data-toggle="modal" data-target="#exampleModalEditar" class="list-group-item list-group-item-action p-2 text-center">EDITAR  <i class="icon ion-md-create"></i></a>
            <a href="" data-toggle="modal" data-target="#exampleModalEliminar" class="list-group-item list-group-item-action p-2 text-center">ELIMINAR  <i class="icon ion-md-trash"></i></a>
        </div>
    </div>
    <div class="col-md-10">
        <div id="containerViewDocInicio" class="container">
            
        </div>
        <div id="containerViewDocCreate" class="container" hidden>
            <div class="row">
              <input type="number" id="idDocente" hidden="hidden" disabled="disabled" value="">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="form-group row">
                                    <label for="inputNameDocente" class="col-sm-2 control-label">Nombre Completo</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputNameDocente" placeholder="Jane Doe" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputHorasDocente" class="col-sm-2 control-label">Horas</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="horasdocente" min="0" max="30" value="0" class="form-control" id="inputHorasDocente" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputHorasHorarioDocente" class="col-sm-2 control-label">Horas Horario</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="horashorariodocente" min="0" max="30" value="0" class="form-control" id="inputHorasHorarioDocente" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputContratacion" class="col-sm-2 control-label">Contratación</label>
                                    <div class="col-sm-8">
                                        <select id="inputContratacion" class="form-control" required>
                                            <option selected value="-1">Selecione ...</option>
                                            <option value="1">DOCENTE</option>
                                            <option value="2">HORARIO</option>
                                            <option value="3">ADMINISTRATIVO</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row" >
                                    <div class="col-sm-offset-2 col-sm-12">
                                        <button id="btnSend" onclick="btnSendDoc()" type="button" class="btn btn-success btn-lg btn-block">Enviar Datos</button>
                                        <button id="btnUpdate" onclick="btnActionDocUpdate()" type="button" class="btn btn-primary btn-lg btn-block" hidden>Actualizar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- Modal Editar-->
<div class="modal fade" id="exampleModalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Docentes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-nameEdit" class="col-form-label">Nombre Docente:</label>
              <select id="recipient-nameEdit" class="form-control">
                  <option selected>Selecione ...</option>
                  @foreach ($collection as $item)
                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                  @endforeach
                </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button onclick="btnActionDocEdit()" type="button" class="btn btn-primary">Editar <i class="icon ion-md-create"></i></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Eliminar-->
<div class="modal fade" id="exampleModalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Docentes Eliminar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-name-delete" class="col-form-label">Nombre Docente:</label>
              <select id="recipient-name-delete" class="form-control">
                  <option selected>Selecione ...</option>
                  @foreach ($collection as $item)
                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                  @endforeach
                </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
            <button onclick="btnActionDocDelete()" type="button" class="btn btn-danger">Si <i class="icon ion-md-trash"></i></button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
@routes
<script src="{{ asset('js/scriptElements.js') }}"></script>
@endsection