@extends('layouts.app')

@section('content')
<h6 class="btn btn-secondary btn-lg btn-block">Docentes del Departamento de Computación</h6>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Nº</th>
        <th scope="col">Nombre</th>
        <th scope="col">Horas</th>
        <th scope="col">Horas Horario</th>
        <th scope="col">Contratación</th>
        <th scope="col">Aciones</th>
      </tr>
    </thead>
    <tbody>
      @php
        $i = 1;
      @endphp
        @foreach ($list as $item)
            <tr>
            <th scope="row">@php echo $i; @endphp</th>
                <td>{{$item->nombre}}</td>
                <td><input id="horasidd{{$item->id}}" type="number" min="0" max="30" value="{{$item->horas}}"/></td>
                <td><input id="horashorarioidd{{$item->id}}" type="number" min="0" max="30" value="{{$item->horashorario}}"/></td>
                <td>{{$item->contratacion}}</td>
                <td><a href="javascript:updatehoursDoc({{$item->id}})" class="btn btn-info"><i class="icon ion-md-build"></i> Modificar</a></td>
              </tr>
              @php
                  $i += 1;
              @endphp
        @endforeach
    </tbody>
  </table>
@endsection
@section('script')
@routes
<script src="{{ asset('js/script.js') }}"></script>
@endsection